#include <stdio.h>
#include <dlfcn.h>

typedef int (*hello_fn)(void);

int main() {
        void *hdl = dlopen("./libtest.so", RTLD_LAZY);
        if (hdl == NULL) {
                fprintf(stderr, "dlopen failed: %s\n", dlerror());
                return 1;
        }

        hello_fn hello = dlsym(hdl, "hello_world");
        if (hello == NULL) {
                fprintf(stderr, "dlsym failed: %s\n", dlerror());
                return 1;
        }

        int ret = hello();
        if (ret != 0) {
                fprintf(stderr, "bad return value: %d\n", ret);
                return 2;
        }

        return 0;
}
